const char html_main[] PROGMEM = R"=====(

<html>
<head>
	<title>Roomba main page</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
	<link href="/style.css" rel="stylesheet">
</head>
<body>
<div class="container">
<p>
	Current Status: STATUS<br>
	Battery Level: BATTERY %
</p>
<div class="d-grid gap-2">
	<a href="/start" class="btn btn-success btn-xl" role="button">Start Cleaning</a>
	<a href="/return" class="btn btn-primary btn-xl" role="button">Return to Dock</a>
	<a href="/stop" class="btn btn-danger btn-xl" role="button">Stop</a>
	<a href="/diagnostics" class="btn btn-outline-secondary btn-xl" role="button">Diagnostics</a>
</div>
<hr>
<pre><font size="-1">VERSION</font></pre>
</div>
</body>
</html>
)=====";

const char html_diagnostics[] PROGMEM = R"=====(

<html>
<head>
	<title>Roomba Diagnostics</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
	<link href="/style.css" rel="stylesheet">
</head>
<body>
<div class="container">
<center><h2>Diagnostics</h2></center>
<table class="table">
<tbody>
	<tr>
		<td>IP Addresses:</td><td>IP_ADDR</td>
		<td>Mac Address:</td><td>MAC_ADDR</td>
	</tr>
	<tr>
		<td>Uptime:</td><td>UPTIME</td>
		<td>Last boot by:</td><td>RESET_REASON</td>
	</tr>
	<tr>
		<td>Charge State</td><td>CHARGE_STATE</td>
		<td>Battery Voltage</td><td>BATTERY_VOLTAGE mV</td>
	</tr>
	<tr>
		<td>Battery Charge</td><td>BATTERY_CHARGE mAh</td>
		<td>Battery Capacity</td><td>BATTERY_CAPACITY mAh</td>
	</tr>
	<tr>
		<td>Battery Current</td><td>BATTERY_CURRENT mA</td>
		<td>Battery Temperature</td><td>BATTERY_TEMP c</td>
	</tr>
</tbody>
</table>
<hr>
<h3>Sensor Diagnostics</h3>
<table class="table">
<tbody>
	<tr>
		<td class="table-BUMP_LEFT">Bump Left</td>
		<td class="table-BUMP_RIGHT">Bump Right</td>
		<td class="table-WHEEL_DROP_LEFT">Wheel Drop Left</td>
		<td class="table-WHEEL_DROP_RIGHT">Wheel Drop Right</td>
	</tr>
	<tr>
		<td class="table-REAL_WALL">Wall</td>
		<td class="table-VIRTUAL_WALL">Virtual Wall</td>
		<td>Dirt Detect: DIRT_DETECT</td>
		<td></td>
	</tr>
	<tr>
		<td class="table-CLIFF_LEFT">Cliff Left</td>
		<td class="table-CLIFF_FRONT_LEFT">Cliff Front Left</td>
		<td class="table-CLIFF_FRONT_RIGHT">Cliff Front Right</td>
		<td class="table-CLIFF_RIGHT">Cliff Right</td>
	</tr>
</tbody>
</table>
<hr>
<p><ul>
	<b>Logs:</b><br>
	<pre>LOGS</pre>
</ul></p>
<hr>
<div class="d-grid gap-2">
	<a href="/" class="btn btn-primary btn-xl" role="button">Return to main page</a>
	<a href="/reset" class="btn btn-outline-primary btn-xl" role="button">Reset Roomba</a>
	<a href="/reboot" class="btn btn-outline-secondary btn-xl" role="button">Reboot</a>
</div>
<hr>
<pre><font size="-1">VERSION</font></pre>
</div>
</body>
</html>
)=====";

const char html_stylesheet[] PROGMEM = R"=====(
.btn-xl {
    padding: 40px 40px;
    font-size: 50px;
    border-radius: 10px;
}
p {
  font-size: 45px;
}
)=====";
