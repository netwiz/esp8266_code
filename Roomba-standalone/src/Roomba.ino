#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266mDNS.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <webpages.h>

// Comment out the following line to build without HTTP autoupdate.
#define WITH_AUTOUPDATE
#ifdef WITH_AUTOUPDATE
#include <../../autoupdate.h>
// Configure the ticker timer.
Ticker updateTicker;
bool run_update = false;
#endif

#define noSleepPin 0
#define ROOMBA_READ_TIMEOUT 200
#define STATE_UNKNOWN 0
#define STATE_CLEANING 1
#define STATE_RETURNING 2
#define STATE_DOCKED 3
#define STATE_IDLE 4
#define RETURN_BATTERY_PERCENT 50	// Battery percent that we return to dock and ignore further 'start' commands.

#define CHARGE_NONE 0			// Not charging
#define CHARGE_RECONDITIONING 1	// Reconditioning
#define CHARGE_BULK 2			// Full Charging
#define CHARGE_TRICKLE 3		// Trickle Charging
#define CHARGE_WAITING 4		// Waiting
#define CHARGE_FAULT 5			// Charging Fault Condition

// Hold the Roomba status
struct Roomba_State {
	uint8_t state;
	uint8_t charge_state;
	uint16_t battery_voltage;
	int16_t battery_current;
	uint8_t battery_temp;
	uint16_t battery_charge;
	uint16_t battery_capacity;
	uint8_t battery_percent;
	uint8_t num_restarts;
	uint8_t num_timeouts;
	byte bump_wheel_drops;
	uint8_t wall;
	uint8_t cliff_left;
	uint8_t cliff_front_left;
	uint8_t cliff_front_right;
	uint8_t cliff_right;
	uint8_t virtual_wall;
	uint8_t wheel_overcurrents;
	uint8_t dirt_detect;
};
struct Roomba_State Roomba;

ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;
Ticker Ticker_getSensors;
bool runGetSensors = false;

// Variables
WiFiEventHandler mConnectHandler, mDisConnectHandler, mGotIpHandler;
long lastReconnectAttempt = 0;
String logbuf;

void onConnected(const WiFiEventStationModeConnected& event){
	logbuf += millis();
	logbuf += ": Connected to AP.";
	logbuf += "\n";
	Roomba.state = STATE_UNKNOWN;
}

void onDisconnect(const WiFiEventStationModeDisconnected& event){
	logbuf += millis();
	logbuf += ": Station disconnected";
	logbuf += "\n";
	WiFi.begin();
}

void onGotIP(const WiFiEventStationModeGotIP& event){
	logbuf += millis();
	logbuf += ": Station connected, IP: ";
	logbuf += WiFi.localIP().toString();
	logbuf += "\n";
	#ifdef WITH_AUTOUPDATE
	// Check for update in 10 seconds time.
	updateTicker.once(10, []() { run_update = true; });
	#endif
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
	myWiFiManager->getConfigPortalSSID();
}

void setup() {
	logbuf.reserve(4096);

	// Setup the GPIO for pulsing the BRC pin.
	pinMode(noSleepPin, OUTPUT);
	digitalWrite(noSleepPin, HIGH);

	// Reset the Roomba.
	Serial.begin(115200);
	resetRoomba();

	mConnectHandler = WiFi.onStationModeConnected(onConnected);
	mDisConnectHandler = WiFi.onStationModeDisconnected(onDisconnect);
	mGotIpHandler = WiFi.onStationModeGotIP(onGotIP);

	// Set power saving for device. WIFI_LIGHT_SLEEP is buggy, so use WIFI_MODEM_SLEEP
	WiFi.setSleepMode(WIFI_MODEM_SLEEP);
	WiFi.setOutputPower(18);	// 10dBm == 10mW, 14dBm = 25mW, 17dBm = 50mW, 20dBm = 100mW

	// Start connecting to wifi.
	WiFiManager WiFiManager;
	WiFiManager.setDebugOutput(false);	// Don't send stuff to the serial port and confuse the roomba!
	if (!WiFiManager.autoConnect()) {
		Serial.println("failed to connect and hit timeout");
		ESP.reset();
		delay(1000);
	}

	httpServer.on("/", handle_root);
	httpServer.on("/diagnostics", handle_diagnostics);
	httpServer.on("/start", handle_start);
	httpServer.on("/stop", handle_stop);
	httpServer.on("/return", handle_return);
	httpServer.on("/reboot", reboot);
	httpServer.on("/reset", handle_reset);
	httpServer.on("/style.css", handle_stylesheet);
	httpUpdater.setup(&httpServer);
	httpServer.begin();

	MDNS.begin("Roomba");
	MDNS.addService("http", "tcp", 80);

	Ticker_getSensors.once(10, []() { runGetSensors = true; });
}

void loop() {
	if ( logbuf.length() > 4000 ) {
		logbuf.remove(0,3000);
	}

	httpServer.handleClient();
	MDNS.update();

	if ( Roomba.num_timeouts > 5 ) { resetRoomba(); }

	if ( runGetSensors ) {
		getSensors();
		runGetSensors = false;
		Ticker_getSensors.once(5, []() { runGetSensors = true; });
	}

	#ifdef WITH_AUTOUPDATE
	if ( run_update ) {
		logbuf += checkForUpdate();
		updateTicker.once(8 * 60 * 60, []() { run_update = true; });
		run_update = false;
	}
	#endif

	delay(50);
}

void getSensors() {
	StayAwake();
	char buffer[10];
	logbuf = "getSensors - Running\n";

	const byte dataPackets [] = {1, 3};
	// Sensor group 1.
	// 7 (1 byte reply) - Bumps and Wheel Drops
	// 8 (1 byte reply) - Wall Sensor
	// 9 (1 byte reply) - Cliff Left
	// 10 (1 byte reply) - Cliff Front Left
	// 11 (1 byte reply) - Cliff Front Right
	// 12 (1 byte reply) - Cliff Right
	// 13 (1 byte reply) - Virtual Wall
	// 14 (1 byte reply) - Wheel overcurrents
	// 15 (1 byte reply) - Dirt Detect
	// 16 (1 byte reply) - Unused (always 0)

	// Sensor group 3.
	// 21 (1 byte reply) - charge state
	// 22 (2 byte reply) - battery voltage
	// 23 (2 byte reply) - battery_current
	// 24 (1 byte reply) - battery_temp
	// 25 (2 byte reply) - battery charge
	// 26 (2 byte reply) - battery capacity

	for (byte packet_id : dataPackets) {
		// Clear any read buffer remaining.
		int i = 0;
		while ( Serial.available() > 0 ) {
			Serial.read();
			i++;
			delay(1);
		}
		if ( i > 0 ) {
			logbuf += "Dumped ";
			logbuf += i;
			logbuf += " bytes.\n";
		}
	
		byte command[] = { 128, 149, 1, packet_id };
		SendCommandList( command, 4 );

		// Allow 25ms for processing...
		delay(25);

		// We should get 10 bytes back.
		i = 0;
		logbuf += "RX: ";
		while ( Serial.available() > 0) {
			buffer[i] = Serial.read();
			logbuf += String(buffer[i], DEC);
			logbuf += " ";
			i++;
			delay(1);
		}
		logbuf += "\n";

		// Handle if the Roomba stops responding.
		if ( i == 0 ) {
			Roomba.num_timeouts++;
			logbuf += "ERROR: No response - Retry: ";
			logbuf += Roomba.num_timeouts;
			logbuf += "\n";
			if ( Roomba.num_timeouts > 10 ) {
				Roomba.state = STATE_UNKNOWN;
			}
			return;
		} else {
			Roomba.num_timeouts = 0;
		}

		// Handle an incomplete packet (too much or too little data)
		if ( i != 10 ) {
			logbuf += "ERROR: Incomplete packet recieved ";
			logbuf += i;
			logbuf += " bytes.\n";
			return;
		}

		switch (packet_id) {
			case 1:
				Roomba.bump_wheel_drops = buffer[0];
				Roomba.wall = buffer[1];
				Roomba.cliff_left = buffer[2];
				Roomba.cliff_front_left = buffer[3];
				Roomba.cliff_front_right = buffer[4];
				Roomba.cliff_right = buffer[5];
				Roomba.virtual_wall = buffer[6];
				Roomba.wheel_overcurrents = buffer[7];
				Roomba.dirt_detect = buffer[8];
				break;

			case 3:
				// Battery / Charge status packet.
				Roomba.charge_state = buffer[0];
				Roomba.battery_voltage = (uint16_t)word(buffer[1], buffer[2]);
				Roomba.battery_current = (int16_t)word(buffer[3], buffer[4]);
				Roomba.battery_temp = buffer[5];
				Roomba.battery_charge = (uint16_t)word(buffer[6], buffer[7]);
				Roomba.battery_capacity = (uint16_t)word(buffer[8], buffer[9]);

				// Sanity check some data...
				if ( Roomba.charge_state > 6 ) { return; }			// Values should be 0-6
				if ( Roomba.battery_capacity == 0 ) { return; }		// We should never get this - but we don't want to divide by zero!
				if ( Roomba.battery_capacity > 6000 ) { return; }	// Usually around 2050 or so.
				if ( Roomba.battery_charge > 6000 ) { return; }		// Can't be greater than battery_capacity
				if ( Roomba.battery_voltage > 18000 ) { return; }	// Should be about 17v on charge, down to ~13.1v when flat.

				uint8_t new_battery_percent = 100 * Roomba.battery_charge / Roomba.battery_capacity;
				if ( new_battery_percent > 100 ) { return; }
				Roomba.battery_percent = new_battery_percent;

				// Reset num_restarts if current draw is over 300mA
				if ( Roomba.battery_current < -300 ) {
					Roomba.num_restarts = 0;
				}

				// Set to a STATE_UNKNOWN when 5 restarts have failed.
				if ( Roomba.num_restarts >= 5 ) {
					Roomba.state = STATE_UNKNOWN;
				}

				// The next two states restart cleaning if battery current is too low do be doing anything.
				if ( Roomba.state == STATE_CLEANING ) {
					if ( Roomba.battery_percent > 10 && Roomba.battery_current > -300 ) {
						Roomba.num_restarts++;
						startCleaning();
					}
				}
				if ( Roomba.state == STATE_RETURNING ) {
					if ( Roomba.battery_percent > 10 && Roomba.battery_current > -300 ) {
						Roomba.num_restarts++;
						return_to_base();
					}
				}

				// The following will only be run if we're in Reconditioning, Bulk charge, or Trickle charge.
				if ( Roomba.charge_state >= CHARGE_RECONDITIONING && Roomba.charge_state <= CHARGE_WAITING ) {
					if ( Roomba.state != STATE_CLEANING ) {
						Roomba.state = STATE_DOCKED;
					}
				}

				// Start seeking the dock if battery gets to RETURN_BATTERY_PERCENT % or below and we're still in STATE_CLEANING
				if ( Roomba.state == STATE_CLEANING && Roomba.battery_percent <= RETURN_BATTERY_PERCENT ) {
					return_to_base();
				}
				break;
		}
	}
	logbuf += "getSensors - Success\n";
}

String translateState() {
	switch (Roomba.state) {
		case STATE_CLEANING: return "Cleaning"; break;
		case STATE_DOCKED: return "Docked"; break;
		case STATE_IDLE: return "Idle"; break;
		case STATE_RETURNING: return "Returning to Dock"; break;
		case STATE_UNKNOWN: return "Error"; break;
	}
	return "Unknown State";
}

String translateChargeState() {
	switch (Roomba.charge_state) {
		case CHARGE_NONE: return "Not Charging"; break;
		case CHARGE_RECONDITIONING: return "Reconditioning"; break;
		case CHARGE_BULK: return "Charging"; break;
		case CHARGE_TRICKLE: return "Trickle Charge"; break;
		case CHARGE_WAITING: return "Charged"; break;
		case CHARGE_FAULT: return "Charging Fault"; break;
	}
	return "Unknown State";
}

void StayAwake() {
	logbuf += "Pulsing the BRC pin...\n";
	digitalWrite(noSleepPin, LOW);
	delay(50);
	digitalWrite(noSleepPin, HIGH);
}

void resetRoomba() {
	Roomba.num_timeouts = 0;
	byte command[] = { 128, 129, 11, 7 };

	// Send factory reset in 19200 baud (sometimes we get stuck in this baud?!)
	StayAwake();
	Serial.begin(19200);
	SendCommandList( command, 4 );

	delay(100);

	// Send factory reset at 115200 baud (we should always be in this - but sometimes it bugs out.)
	StayAwake();
	Serial.begin(115200);
	SendCommandList( command, 4 );
}

char *uptime(unsigned long milli) {
	static char _return[32];
	unsigned long secs=milli/1000, mins=secs/60;
	unsigned int hours=mins/60, days=hours/24;
	milli-=secs*1000;
	secs-=mins*60;
	mins-=hours*60;
	hours-=days*24;
	sprintf(_return,"%d days %2.2d:%2.2d:%2.2d.%3.3d", (byte)days, (byte)hours, (byte)mins, (byte)secs, (int)milli);
	return _return;
}

void handle_root() {
	String webpage = html_main;
	webpage.replace("STATUS", translateState());
	webpage.replace("BATTERY", String(Roomba.battery_percent));
	webpage.replace("VERSION", ESP.getFullVersion());
	httpServer.sendHeader("Refresh", "10");
	httpServer.send(200, "text/html", webpage);
}

void handle_diagnostics() {
	String ip_addr = "";
	for (auto a : addrList) {
		ip_addr += a.toString().c_str();
		ip_addr += "<br>";
	}
	char buffer [4];
	bool BitArray[8];
	for (int i = 0; i < 8; i++)
    	BitArray[i] = bitRead(Roomba.bump_wheel_drops, i);

	String webpage = html_diagnostics;
	webpage.replace("IP_ADDR", ip_addr);
	webpage.replace("MAC_ADDR", WiFi.macAddress().c_str());
	webpage.replace("UPTIME", uptime(millis()));
	webpage.replace("RESET_REASON", ESP.getResetReason());
	webpage.replace("CHARGE_STATE", translateChargeState());
	webpage.replace("BATTERY_VOLTAGE", itoa(Roomba.battery_voltage, buffer, DEC));
	webpage.replace("BATTERY_CHARGE", itoa(Roomba.battery_charge, buffer, DEC));
	webpage.replace("BATTERY_CAPACITY", itoa(Roomba.battery_capacity, buffer, DEC));
	webpage.replace("BATTERY_CURRENT", itoa(Roomba.battery_current, buffer, DEC));
	webpage.replace("BATTERY_TEMP", itoa(Roomba.battery_temp, buffer, DEC));
	webpage.replace("LOGS", logbuf);
	webpage.replace("VERSION", ESP.getFullVersion());

	// Roomba status values here
	webpage.replace("BUMP_LEFT", BitArray[1]?"danger":"light");
	webpage.replace("BUMP_RIGHT", BitArray[0]?"danger":"light");
	webpage.replace("WHEEL_DROP_LEFT", BitArray[3]?"danger":"light");
	webpage.replace("WHEEL_DROP_RIGHT", BitArray[2]?"danger":"light");
	if ( Roomba.wall == 1 ) {
		webpage.replace("REAL_WALL", "danger");
	} else {
		webpage.replace("REAL_WALL", "light");
	}
	if ( Roomba.cliff_left == 1 ) {
		webpage.replace("CLIFF_LEFT", "danger");
	} else {
		webpage.replace("CLIFF_LEFT", "light");
	}
	if ( Roomba.cliff_front_left == 1 ) {
		webpage.replace("CLIFF_FRONT_LEFT", "danger");
	} else {
		webpage.replace("CLIFF_FRONT_LEFT", "light");
	}
	if ( Roomba.cliff_front_right == 1 ) {
		webpage.replace("CLIFF_FRONT_RIGHT", "danger");
	} else {
		webpage.replace("CLIFF_FRONT_RIGHT", "light");
	}
	if ( Roomba.cliff_right == 1 ) {
		webpage.replace("CLIFF_RIGHT", "danger");
	} else {
		webpage.replace("CLIFF_RIGHT", "light");
	}
	if ( Roomba.virtual_wall == 1 ) {
		webpage.replace("VIRTUAL_WALL", "danger");
	} else {
		webpage.replace("VIRTUAL_WALL", "light");
	}
	webpage.replace("DIRT_DETECT", itoa(Roomba.dirt_detect, buffer, 10));

	httpServer.sendHeader("Refresh", "5");
	httpServer.send(200, "text/html", webpage);
}

void handle_start() {
	startCleaning();
	httpServer.sendHeader("Location", "/");
	httpServer.send(307, "text/html" "Starting...");
}

void handle_stop() {
	stopCleaning();
	httpServer.sendHeader("Location", "/");
	httpServer.send(307, "text/html" "Stopping...");
}

void handle_return() {
	return_to_base();
	httpServer.sendHeader("Location", "/");
	httpServer.send(307, "text/html" "Returning...");
}

void handle_reset() {
	resetRoomba();
	httpServer.sendHeader("Location", "/diagnostics");
	httpServer.send(307, "text/html" "Resetting...");
}

void handle_stylesheet() {
	String webpage = html_stylesheet;
	httpServer.send(200, "text/css", webpage);
}

void reboot() {
	String webpage = "<html><head><meta http-equiv=\"refresh\" content=\"10;url=/\"></head><body>Rebooting</body></html>";
	httpServer.send(200, "text/html", webpage);
	httpServer.handleClient();
	delay(100);
	ESP.restart();
}

// Send commands:
// 135 = Clean
// 136 = Start Max Cleaning Mode
void startCleaning() {
	Roomba.state = STATE_CLEANING;
	byte command[] = { 128, 131, 135 };
	SendCommandList( command, 3 );
}

// Send commands:
// 143 = Seek Dock
void return_to_base() {
	// IF we're already cleaning, sending this once will only stop the Roomba.
	// We have to send twice to actually do something...
	if ( Roomba.state == STATE_CLEANING ) {
		byte command[] = { 128, 131, 143 };
		SendCommandList( command, 3 );
		delay(250);
	}
	Roomba.state = STATE_RETURNING;
	byte command[] = { 128, 131, 143 };
	SendCommandList( command, 3 );
}

// Send commands:
// 143 = Seek Dock
void stopCleaning() {
	Roomba.state = STATE_IDLE;
	byte command[] = { 128, 131, 143 };
	SendCommandList( command, 3 );
}

void SendCommandList( byte *ptr, byte len ) {
	logbuf += "TX:";
	for ( int i = 0; i < len ; i++ ) {
		logbuf += " ";
		logbuf += ptr[i];
		Serial.write(ptr[i]);
	}
	logbuf += "\n";
}
